package com.atlassian.html.encode;

import org.junit.Test;

import java.io.IOException;
import java.io.StringWriter;

import static org.junit.Assert.assertEquals;

public class TestJavascriptEncoder
{
    @Test
    public void testEmpty() throws IOException
    {
        assertEncoding("", "");
    }

    @Test
    public void testBasic() throws IOException
    {
        assertEncoding("a", "a");
    }

    @Test
    public void testHtml() throws IOException
    {
        assertEncoding("<h1>", "\\u003ch1\\u003e");
    }

    @Test
    public void testScriptClose() throws IOException
    {
        assertEncoding("</script>", "\\u003c/script\\u003e");
    }

    @Test
    public void testBackslash() throws IOException
    {
        assertEncoding("\\", "\\u005C");
    }

    @Test
    public void testSingleQuote() throws IOException
    {
        assertEncoding("'", "\\u0027");
    }

    @Test
    public void testDoubleQuote() throws IOException
    {
        assertEncoding("\"", "\\u0022");
    }

    private void assertEncoding(String input, String expected) throws IOException
    {
        StringWriter writer = new StringWriter();
        JavascriptEncoder.escape(writer, input);
        assertEquals(expected, writer.toString());
    }
}
