package com.atlassian.html.encode;

import org.junit.Test;

import java.io.StringWriter;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 */
public class HtmlEncoderTest
{
    private static final String QFOX_1 = "The quick & brown fox jumped > the lazy dog";
    private static final String QFOX_EXPECTED = "The quick &amp; brown fox jumped &gt; the lazy dog";

    private static final String QFOX_PASSTHRU = "The quick brown fox jumped over the lazy dog";

    @Test
    public void testEncode() throws Exception
    {

        assertThat(HtmlEncoder.encode(QFOX_1), equalTo(QFOX_EXPECTED));

        assertThat(HtmlEncoder.encode(QFOX_PASSTHRU), equalTo(QFOX_PASSTHRU));
    }

    @Test
    public void testEncodeWriter() throws Exception
    {
        StringWriter sw;

        sw = new StringWriter();
        HtmlEncoder.encode(sw, QFOX_1.toCharArray(), 0, QFOX_1.length());
        assertThat(sw.toString(), equalTo(QFOX_EXPECTED));

        sw = new StringWriter();
        HtmlEncoder.encode(sw, QFOX_PASSTHRU.toCharArray(), 0, QFOX_PASSTHRU.length());
        assertThat(sw.toString(), equalTo(QFOX_PASSTHRU));


    }
}
