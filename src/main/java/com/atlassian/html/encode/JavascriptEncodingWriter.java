package com.atlassian.html.encode;

import com.atlassian.annotations.PublicApi;

import java.io.IOException;
import java.io.Writer;

/**
 * Writer that JS-encodes all content
 * @since v6.0
 */
@PublicApi
public class JavascriptEncodingWriter extends Writer
{
    private final Writer writer;

    public JavascriptEncodingWriter(Writer writer)
    {
        this.writer = writer;
    }

    @Override
    public void write(char[] chars, int off, int len) throws IOException
    {
        JavascriptEncoder.escape(writer, chars, off, len);
    }

    @Override
    public void flush() throws IOException
    {
        writer.flush();
    }

    @Override
    public void close() throws IOException
    {
        writer.close();
    }
}
